from ConfigParser import SafeConfigParser
from src.game import Game
from src.constants import game_const

config = SafeConfigParser()
config.read(game_const.CONFIG_FILE)

game = Game(config)
game.start_game()
