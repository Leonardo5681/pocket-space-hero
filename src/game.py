import psp2d
import stackless
import pspmp3

from src import input_handler
from src.render import Render
from src.updater import Updater
from src.scene.collision_mask import CollisionMask
from src.game_state import GameState
from src.constants import game_const

IMAGE_CONFIG_FILE = "IMAGES"
FONT_CONFIG_FILE = "FONT"
MUSIC_CONFIG_FILE = "MUSIC"


class Game:
    """
    This class initializes various configuration settings and handles the game cycle.

    Attributes
    ----------
    action: str
        The action which has been give in input by the player.
    d_pad: str
        The direction which has been give in input by the player.
    updater: Updater
        It executes the game logic and updates its state.
    render: Render
        It renders the scene.
    background: Image
        The background image.
    game_title: Image
        The game title image.
    life: Image
        The life image.
    game_font: Font
        The font used to display messages and labels.
    """

    def __init__(self, config):
        """
        Parameters
        ----------
        config: SafeConfigParser
            A parser for the configuration file.
        """
        self.config = config
        self.background = psp2d.Image(config.get(IMAGE_CONFIG_FILE, "BackgroundImage"))
        self.game_title = psp2d.Image(config.get(IMAGE_CONFIG_FILE, "GameTitle"))
        self.life = psp2d.Image(config.get(IMAGE_CONFIG_FILE, "Life"))
        self.game_font = psp2d.Font(config.get(FONT_CONFIG_FILE, "Font"))

        # 0 initial sprite, 1 right, 2 left, 3 damaged, 4 right damaged, 5 left damaged
        self.player_sprites = (
            psp2d.Image(config.get(IMAGE_CONFIG_FILE, "Player")),
            psp2d.Image(config.get(IMAGE_CONFIG_FILE, "PlayerRight")),
            psp2d.Image(config.get(IMAGE_CONFIG_FILE, "PlayerLeft")),
            psp2d.Image(config.get(IMAGE_CONFIG_FILE, "PlayerDamaged")),
            psp2d.Image(config.get(IMAGE_CONFIG_FILE, "PlayerDamagedRight")),
            psp2d.Image(config.get(IMAGE_CONFIG_FILE, "PlayerDamagedLeft")),)
        # create collision masks
        self.player_masks = [CollisionMask(player_sprite) for player_sprite in self.player_sprites]

        # 0 shield sprite
        self.power_ups_sprites = (
            psp2d.Image(config.get(IMAGE_CONFIG_FILE, "Shield")),)
        # create collision masks
        self.power_ups_masks = [CollisionMask(self.power_ups_sprites[0], 15)]

        self.enemy_sprites = (psp2d.Image(config.get(IMAGE_CONFIG_FILE, "Enemy")),)
        self.enemy_masks = [CollisionMask(self.enemy_sprites[0])]

        self.laser_sprites = (psp2d.Image(config.get(IMAGE_CONFIG_FILE, "PlayerLaser")),)
        self.laser_masks = [CollisionMask(self.laser_sprites[0])]

        self.enemy_laser_sprites = (psp2d.Image(config.get(IMAGE_CONFIG_FILE, "EnemyLaser")),)
        self.enemy_laser_masks = [CollisionMask(self.enemy_laser_sprites[0])]

        self.music_file = config.get(MUSIC_CONFIG_FILE, "BackgroundMusic")

        self.action = None
        self.d_pad = None
        self.render = Render()

        pspmp3.init(2)
        pspmp3.load(self.music_file)

    def start_game(self):
        """
        Starts the game.

        Returns
        -------
        None
        """
        pspmp3.play()

        action = self.__start_screen()
        while action == game_const.START:
            self.__run_cycle()
            action = self.__end_screen()

        pspmp3.end()

    def __run_cycle(self):
        """
        Runs the game cycle.

        Returns
        -------
        None
        """
        self.action = None
        self.d_pad = None
        self.game_state = GameState()
        self.updater = Updater()
        # switch the state of the game to PLAY
        self.game_state.state = game_const.PLAY

        while self.game_state.state == game_const.PLAY:
            stackless.tasklet(self.__get_user_input)()
            stackless.schedule()

            stackless.tasklet(self.__update_game)()
            stackless.schedule()

            stackless.tasklet(self.__render_game)()
            stackless.schedule()

            stackless.tasklet(self.__check_background_music)()
            stackless.schedule()

        stackless.run()

    def __get_user_input(self):
        """
        Gets the player input.

        Returns
        -------
        None
        """
        self.d_pad = input_handler.get_d_pad_input()
        self.action = input_handler.get_action_buttons_input()

    def __update_game(self):
        """
        Updates the game state using an Updater.

        Returns
        -------
        None
        """
        self.game_state = self.updater.update_power_ups(self.game_state)

        self.game_state = self.updater.update_enemies(self.game_state)

        self.game_state = self.updater.update_player(self.game_state, self.d_pad, self.action)

        self.game_state = self.updater.update_lasers(self.game_state)

        self.game_state = self.updater.process_power_ups_collisions_player(self.game_state,
                                                                           self.player_masks[
                                                                               self.game_state.player.curr_sprite],
                                                                           self.power_ups_masks
                                                                           )

        self.game_state = self.updater.process_laser_collisions_enemies(self.game_state, self.enemy_masks[0],
                                                                        self.laser_masks[0])

        self.game_state = self.updater.process_player_collisions_enemies(self.game_state,
                                                                         self.player_masks[
                                                                             self.game_state.player.curr_sprite],
                                                                         self.enemy_masks[0])

        self.game_state = self.updater.process_laser_collisions_player(self.game_state,
                                                                       self.player_masks[
                                                                           self.game_state.player.curr_sprite],
                                                                       self.enemy_laser_masks[0]
                                                                       )

        self.game_state = self.updater.check_game_state(self.game_state)

    def __render_game(self):
        """
        Renders the current scene using a Render.

        Returns
        -------
        None
        """
        self.render.render_scene(self.game_state, self.background, self.life, self.player_sprites,
                                 self.laser_sprites,
                                 self.enemy_sprites,
                                 self.enemy_laser_sprites, self.power_ups_sprites, self.game_font)

    def __end_screen(self):
        """
        Renders the game over scene using a Render.

        Returns
        -------
        str:
            A string which represents the action the button pressed by the player.
        """
        self.render.end_game(self.game_state.score, self.background, self.game_title, self.game_font)
        action = None
        # until the player presses START
        while action is None:
            if input_handler.get_start_button() == game_const.START:
                action = game_const.START
            self.__check_background_music()
        return action

    def __check_background_music(self):
        """
        Check if the stream has reached the end.

        Returns
        -------
        None
        """
        if pspmp3.endofstream() == 1:
            pspmp3.end()

    def __start_screen(self):
        """
        Renders the start game scene using a Render.

        Returns
        -------
        str:
            A string which represents the action the button pressed by the player.
        """
        self.render.init_game(self.background, self.game_title, self.game_font)
        action = None
        # until the player presses START
        while action is None:
            if input_handler.get_start_button() == game_const.START:
                action = game_const.START
            self.__check_background_music()
        return action
