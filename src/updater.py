import random
import time

from src.scene.enemy import Enemy
from src.scene.laser import Laser
from src.constants import game_const
from src.scene.shield import Shield


class Updater:
    """
    This class is responsible for executing the game logic and updating its state.

    Attributes
    ----------
    last_enemy_spawn: float
        Time when the last enemy was spawned
    time_action: float
        Time when the last action was performed by the player
    """

    def __init__(self):
        self.last_enemy_spawn = 0
        self.time_action = 0
        self.enemies_spawn_time = 1.5
        # it prevents from creating a new power up at the beginning of the game
        self.last_power_up_time = time.time()
        # the time which must pass to create a new power up
        self.power_up_time = random.randrange(10, 25)

    def update_power_ups(self, game_state):
        """
        Add new power ups on the screen.

        Parameters
        ----------
        game_state: GameState
            The game state.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        curr_time = time.time()

        if (curr_time - self.last_power_up_time >= self.power_up_time and
                len(game_state.power_ups) < game_const.MAX_NUM_POWER_UPS):
            shield_pos_x = random.randrange(45, 415)
            shield_pos_y = random.randrange(45, 125)
            game_state.power_ups.append(Shield(shield_pos_x, shield_pos_y))
            self.last_power_up_time = curr_time
            self.power_up_time = random.randrange(10, 25)

        return game_state

    def update_enemies(self, game_state):
        """
        Creates new enemies and updates the existing ones. It also creates lasers fired by enemies.

        Parameters
        ----------
        game_state: GameState
            The game state.
        Returns
        -------
        game_state: GameState
            The game state.
        """
        curr_time = time.time()

        if not game_state.enemies or ((curr_time - self.last_enemy_spawn >= self.enemies_spawn_time)
                                      and (len(game_state.enemies) < game_const.MAX_NUM_ENEMIES
                                           and game_state.enemies_level > 0)):
            # create an enemy in a random position on the screen with a random direction (right or left)
            game_state.enemies.append(Enemy(random.randrange(45, 415), random.randrange(45, 125),
                                            random.choice([game_const.RIGHT, game_const.LEFT])))
            self.last_enemy_spawn = curr_time

        for enemy in game_state.enemies:
            enemy.move()
            curr_time = time.time()
            if curr_time - enemy.fire_time >= 1.5:
                enemy.fire_time = curr_time
                laser_pos_x = enemy.pos_x + game_const.ENEMY_W / 2 - 1
                laser_pos_y = enemy.pos_y
                game_state.enemy_lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.DOWN))
        return game_state

    def update_player(self, game_state, d_pad, action):
        """
        Updates the player and creates a laser if fired by the player.

        Parameters
        ----------
        game_state: GameState
            The game state.
        d_pad: str
            The direction where to move the player.
        action: str
            The action of the player.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        curr_time = time.time()
        game_state.player.move(d_pad)
        # check if the player has to perform an action and it's passed enough time
        if action != game_const.NONE and (curr_time - self.time_action) >= 0.2:
            if action == game_const.FIRE:
                laser_pos_x = game_state.player.pos_x + game_const.PLAYER_W / 2 - 1
                laser_pos_y = game_state.player.pos_y
                game_state.lasers.append(Laser(laser_pos_x, laser_pos_y, game_const.UP))
                self.time_action = curr_time
        return game_state

    def update_lasers(self, game_state):
        """
        Updates the game state by removing the lasers which are outside the boundaries of the screen.

        Parameters
        ----------
        game_state: GameState
            The game state.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        for laser in game_state.lasers:
            laser.move()
            if laser.pos_y > 0:
                tmp_lasers.append(laser)
        game_state.lasers = tmp_lasers

        tmp_lasers = []
        for laser in game_state.enemy_lasers:
            laser.move()
            if laser.pos_y < 480:
                tmp_lasers.append(laser)

        game_state.enemy_lasers = tmp_lasers
        return game_state

    def process_player_collisions_enemies(self, game_state, player_mask, enemy_mask):
        """
        Checks collisions between the player and enemies.

        Parameters
        ----------
        game_state: GameState
            The game state.
        player_mask: CollisionMask
            The collision mask of the player sprite.
        enemy_mask: CollisionMask
            The collision mask of the enemy sprite.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        for enemy in game_state.enemies:
            if player_mask.collide(enemy_mask, game_state.player.pos_x, game_state.player.pos_y, enemy.pos_x,
                                   enemy.pos_y) > 0:
                game_state.player.respawn()
                game_state.player.lives = game_state.player.lives - 1
                break
        return game_state

    def process_laser_collisions_enemies(self, game_state, enemy_mask, laser_mask):
        """
        Checks collisions between lasers fired by the player and enemies.

        Parameters
        ----------
        game_state: GameState
            The game state.
        enemy_mask: CollisionMask
            The collision mask of the enemy sprite.
        laser_mask: CollisionMask
            The collision mask of the player laser sprite.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        for laser in game_state.lasers:
            collision = False
            tmp_enemy = None
            for enemy in game_state.enemies:
                if enemy_mask.collide(laser_mask,
                                      enemy.pos_x, enemy.pos_y,
                                      laser.pos_x,
                                      laser.pos_y) > 0:
                    # save temporary destroyed enemy
                    collision = True
                    tmp_enemy = enemy
                    break
            if collision:
                game_state.enemies.remove(tmp_enemy)
                game_state.score += game_const.POINTS_INCREMENT
                game_state.enemies_level -= 1
            else:
                tmp_lasers.append(laser)

        game_state.lasers = tmp_lasers
        return game_state

    def process_laser_collisions_player(self, game_state, player_mask, enemy_laser_mask):
        """
        Checks collisions between lasers fired by enemies and the player.

        Parameters
        ----------
        game_state: GameState
            The game state.
        player_mask: CollisionMask
            The collision mask of the player sprite.
        enemy_laser_mask: CollisionMask
            The collision mask of the laser enemy sprite.

        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_lasers = []
        tmp_player_lives = game_state.player.lives
        tmp_player_damaged = game_state.player.damaged

        for laser in game_state.enemy_lasers:

            if player_mask.collide(enemy_laser_mask, game_state.player.pos_x, game_state.player.pos_y, laser.pos_x,
                                   laser.pos_y) > 0:
                if game_state.player.shield and game_state.player.shield.strength > 0:
                    game_state.player.shield.strength -= 1
                elif not game_state.player.damaged:
                    game_state.player.damaged = True
                    game_state.player.curr_sprite = 3
                else:
                    game_state.player.lives = game_state.player.lives - 1
                    game_state.player.respawn()
                    game_state.player.damaged = False
                    game_state.player.destroyed = True
            # no damage or player destroyed
            if game_state.player.lives == tmp_player_lives and tmp_player_damaged == game_state.player.damaged:
                tmp_lasers.append(laser)

        game_state.enemy_lasers = tmp_lasers
        return game_state

    def process_power_ups_collisions_player(self, game_state, player_mask, power_ups_masks):
        """
        Checks collisions between power ups and the player.

        Parameters
        ----------
        game_state: GameState
            The game state.
        player_mask: CollisionMask
            The collision mask of the player sprite.
        power_ups_masks: CollisionMask
            The collision masks of the power ups

        Returns
        -------
        game_state: GameState
            The game state.
        """
        tmp_power_ups = []

        for power_up in game_state.power_ups:
            index_mask = 0
            if isinstance(power_up, Shield):
                index_mask = 0
            if player_mask.collide(power_ups_masks[index_mask], game_state.player.pos_x, game_state.player.pos_y,
                                   power_up.pos_x,
                                   power_up.pos_y) > 0:
                if isinstance(power_up, Shield):
                    power_up.pos_x = game_state.player.pos_x - 10
                    power_up.pos_y = game_state.player.pos_y - 10
                    game_state.player.shield = power_up
            else:
                tmp_power_ups.append(power_up)
        game_state.power_ups = tmp_power_ups
        return game_state

    def check_game_state(self, game_state):
        """
        Checks if the game can continue or is over.

        Parameters
        ----------
        game_state: GameState
            The game state.

        Returns
        -------
        str:
            The game state
        """

        if game_state.player.lives <= 0:
            game_state.state = game_const.END
            return game_state
        else:
            if game_state.enemies_level == 0:
                game_state.next_level()
                if self.enemies_spawn_time > 1.0:
                    self.enemies_spawn_time -= 0.1
            game_state.state = game_const.PLAY
            return game_state
