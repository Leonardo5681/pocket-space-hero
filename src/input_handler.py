import psp2d
from src.constants import game_const


def get_d_pad_input():
    """
    Gets the directional button pressed by the player and returns a translated direction string.

    Returns
    -------
    str:
        A string which represents the direction.
    """
    pad = psp2d.Controller()
    if pad.up:
        return game_const.UP
    elif pad.right:
        return game_const.RIGHT
    elif pad.down:
        return game_const.DOWN
    elif pad.left:
        return game_const.LEFT
    return game_const.NONE


def get_start_button():
    """
    Returns a translated string when the start button is pressed by the player.

    Returns
    -------
    str:
        A string which represents the action for the start button.
    """
    pad = psp2d.Controller()
    if pad.start:
        return game_const.START

def get_action_buttons_input():
    """
    Gets the action button pressed by the player and returns a translated action string.

    Returns
    -------
    str:
        A string which represents the action.
    """
    pad = psp2d.Controller()
    if pad.cross:
        return game_const.FIRE
    return game_const.NONE
