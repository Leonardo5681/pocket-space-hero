from src.scene.player import Player
from src.constants import game_const


class GameState:
    """
    This class provides attributes to represent the state of the game.

    Attributes
    ----------
    enemies: list
        Holds the enemies which are on the screen.
    level: int
        Current level of the game.
    score: int
        The score.
    enemy_lasers: list
        Hold the lases which have been fired by the enemies and still on the screen.
    lasers: list
        Hold the lases which have been fired by the player and still on the screen.
    player: Player
        The player.
    state: str
        The state of the game.
    """

    def __init__(self):
        self.enemies = []
        self.level = 1
        self.enemies_level = 10
        self.score = 0
        self.enemy_lasers = []
        self.lasers = []
        # the player is created using its initial coordinates
        self.player = Player(game_const.INIT_POS_X, game_const.INIT_POS_Y)
        # the game starts with a state which is equal to INIT
        self.state = game_const.INIT
        self.power_ups = []

    def next_level(self):
        self.enemies = []
        self.level += 1
        self.enemies_level = self.level * game_const.ENEMIES_MULTIPLIER
        self.enemy_lasers = []
        self.lasers = []
