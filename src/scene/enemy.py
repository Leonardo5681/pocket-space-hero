from src.scene.element import Element
from src.constants import game_const


class Enemy(Element):
    """
    This class implements the enemy character.

    Attributes
    ----------
    fire_time: float
        Time when the enemy last fired.
    direction: str
        Current direction of the enemy.
    """

    def __init__(self, pos_x, pos_y, direction):
        Element.__init__(self, pos_x, pos_y)
        self.fire_time = 0
        self.direction = direction

    def move(self, direction=None):
        if self.direction == game_const.RIGHT:
            if self.pos_x <= 450:
                self.pos_x += game_const.X_ENEMY_SPEED
            else:
                # Switch direction
                self.direction = game_const.LEFT
                self.pos_x -= game_const.X_ENEMY_SPEED
        elif self.direction == game_const.LEFT:
            if self.pos_x >= 0:
                self.pos_x -= game_const.X_ENEMY_SPEED
            else:
                # Switch direction
                self.direction = game_const.RIGHT
                self.pos_x += game_const.X_ENEMY_SPEED
