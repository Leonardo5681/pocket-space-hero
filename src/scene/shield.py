from src.scene.element import Element
from src.constants import game_const


class Shield(Element):
    """
    This class implements a shield which prevents the player being damaged by the enemy lasers.

    Attributes
    ----------
    strength: int
        Strength of the shield.
    """

    def __init__(self, pos_x, pos_y):
        Element.__init__(self, pos_x, pos_y)
        self.strength = game_const.SHIELD_STRENGTH
