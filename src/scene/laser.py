from src.scene.element import Element
from src.constants import game_const


class Laser(Element):
    """
    This class implements a laser which can be fired by the player or an enemy.

    Attributes
    ----------
    direction: str
        Current direction of the laser.
    """

    def __init__(self, pos_x, pos_y, direction):
        Element.__init__(self, pos_x, pos_y)
        self.direction = direction

    def move(self, direction=None):
        if self.direction == game_const.UP:
            self.pos_y -= game_const.Y_LASER_SPEED
        elif self.direction == game_const.DOWN:
            self.pos_y += game_const.Y_LASER_SPEED
