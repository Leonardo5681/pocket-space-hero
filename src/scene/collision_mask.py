class CollisionMask:
    """
    A class representing a binary mask for collision detection.

    Attributes:
    -----------
    w : int
        The width of the mask.
    h : int
        The height of the mask.
    mask : list
        A 2D list representing the binary mask.
    """

    def __init__(self, img, threshold=127):
        """
        Constructs a new Mask object.

        Parameters:
        -----------
        img : class
            An image object with a getPixel() method that returns a Color object.
        threshold : int, optional
            The threshold for considering a pixel as opaque, defaults to 127.
        """
        # Set the width and height attributes
        self.w = img.width
        self.h = img.height

        # Create a 2D list to represent the binary mask
        self.mask = self.__create_mask(img, threshold)

    def __create_mask(self, img, threshold):
        """
        Creates a binary mask from an image.

        Parameters:
        -----------
        img : class
            An image object with a getPixel() method that returns a Color object.
        threshold : int
            The threshold for considering a pixel as opaque.

        Returns:
        --------
        list
            A 2D list representing the binary mask.
        """
        # Create an empty 2D list to represent the binary mask
        mask = [[0 for j in range(img.width)] for i in range(img.height)]

        # Loop over each pixel in the image
        for i in range(img.height):
            for j in range(img.width):
                # Get the pixel color at the current position
                color = img.getPixel(j, i)
                alpha = color.alpha
                # Set the corresponding pixel in the mask to 1 if the alpha value is greater than the threshold
                mask[i][j] = 1 if alpha > threshold else 0

        # Return the binary mask
        return mask

    def collide(self, other_mask, x, y, other_x, other_y):
        """
        Checks if this mask collides with the given mask.

        Parameters:
        -----------
        other_mask : Mask
            The mask to check collision with.
        x : int
            The x-coordinate of the top-left corner of this mask.
        y : int
            The y-coordinate of the top-left corner of this mask.
        other_x : int
            The x-coordinate of the top-left corner of the other mask.
        other_y : int
            The y-coordinate of the top-left corner of the other mask.

        Returns:
        --------
        int
            The number of overlapping pixels between the two masks.
        """
        # Initialize the overlap counter to 0
        overlap = 0

        # Determine the bounds of the overlapping region
        min_x = max(x, other_x)
        max_x = min(x + self.w, other_x + other_mask.w)
        min_y = max(y, other_y)
        max_y = min(y + self.h, other_y + other_mask.h)

        # Loop over each pixel in the overlapping region
        for i in range(min_y, max_y):
            for j in range(min_x, max_x):
                # Check if both masks have a non-zero value at the current position
                if self.mask[i - y][j - x] == 1 and other_mask.mask[i - other_y][j - other_x] == 1:
                    # If so, increment the overlap counter
                    overlap += 1

        # Return the overlap counter
        return overlap
