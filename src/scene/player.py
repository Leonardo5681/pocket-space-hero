from src.scene.element import Element
from src.constants import game_const


class Player(Element):
    """
    This class implements the player.

    Attributes
    ----------
    last_move: str
        Last move of the player.
    last_action: str
        Last action of the player.
    lives: int
        Number of lives.
    damaged: bool
        If the player is damaged.
    destroyed: bool
        If the player is destroyed.
    """

    def __init__(self, pos_x, pos_y):
        Element.__init__(self, pos_x, pos_y)
        self.last_move = game_const.NONE
        self.last_action = game_const.NONE
        self.lives = 3
        self.damaged = False
        self.destroyed = False
        self.shield = None

    def move(self, direction=None):
        if direction == game_const.UP and self.pos_y >= 0:
            self.pos_y -= game_const.Y_PLAYER_SPEED
            self.__set_curr_sprite(3, 0)
        elif direction == game_const.DOWN and self.pos_y <= 245:
            self.pos_y += game_const.Y_PLAYER_SPEED
            self.__set_curr_sprite(3, 0)
        elif direction == game_const.LEFT and self.pos_x >= 0:
            self.pos_x -= game_const.X_PLAYER_SPEED
            self.__set_curr_sprite(5, 2)
        elif direction == game_const.RIGHT and self.pos_x <= 450:
            self.pos_x += game_const.X_PLAYER_SPEED
            self.__set_curr_sprite(4, 1)
        else:
            self.__set_curr_sprite(3, 0)
        if self.shield and self.shield.strength > 0:
            self.shield.pos_x = self.pos_x - 10
            self.shield.pos_y = self.pos_y - 10

    def respawn(self):
        self.pos_x = game_const.INIT_POS_X
        self.pos_y = game_const.INIT_POS_Y
        self.curr_sprite = 0
        self.shield = None

    def __set_curr_sprite(self, damaged_sprite, sprite):
        self.curr_sprite = damaged_sprite if self.damaged else sprite
