class Element:
    """
    Base class to implement characters of the game.

    Attributes
    ----------
    pos_x: int
        The x coordinate of the character.
    pos_y: int
        The y coordinate of the character.
    curr_sprite: int
        An index which is used to get the current sprite and its collision mask.
    """

    def __init__(self, pos_x, pos_y):
        """
        Parameters
        ----------
        pos_x: int
            The initial x coordinate of the character.
        pos_y: int
            The initial y coordinate of the character.
        """
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.curr_sprite = 0

    def move(self, direction=None):
        """
        Moves the character

        Parameters
        ----------
        direction: str
            The direction where the character will be moved (default is None).

        Returns
        -------
        None
        """
        pass

    def respawn(self):
        """
        Creates again the character.

        Returns
        -------
        None
        """
        pass
