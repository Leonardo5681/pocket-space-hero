import psp2d
from src.constants import ui_const


class Render:
    """
    This class is responsible for rendering a scene on the screen.

    Attributes
    ----------
    screen: Screen
        It provides with methods to handle and display graphics on the screen.
    """

    def __init__(self):
        self.screen = psp2d.Screen()

    def render_scene(self, game_state, background, life, player_sprites, laser_sprites, enemy_sprites,
                     enemy_laser_sprites, power_ups_sprites, game_font):
        """
        Renders the current scene of the game.

        Parameters
        ----------
        game_state: GameState
            The state of the game.
        background: Image
            The background image.
        life: Image
            The life image.
        player_sprites: list
            A list which holds the sprites of the player.
        laser_sprites: list
            A list which holds the sprites of the player laser.
        enemy_sprites: list
            A list which holds the sprites of the enemy character.
        enemy_laser_sprites: list
            A list which holds the sprites of the enemy laser.
        power_ups_sprites: list
            A list which holds the sprites of the power ups.
        game_font: Font
            The font used to display messages and labels.

        Returns
        -------
        None
        """
        self.screen.blit(background)
        for enemy in game_state.enemies:
            sprite = enemy_sprites[enemy.curr_sprite]
            self.screen.blit(sprite, 0, 0, sprite.width, sprite.height, enemy.pos_x, enemy.pos_y, True)
        for laser in game_state.lasers:
            sprite = laser_sprites[laser.curr_sprite]
            self.screen.blit(sprite, 0, 0, sprite.width, sprite.height, laser.pos_x, laser.pos_y, True)
        for laser in game_state.enemy_lasers:
            sprite = enemy_laser_sprites[laser.curr_sprite]
            self.screen.blit(sprite, 0, 0, sprite.width, sprite.height, laser.pos_x, laser.pos_y, True)
        for power_up in game_state.power_ups:
            sprite = power_ups_sprites[power_up.curr_sprite]
            self.screen.blit(sprite, 0, 0, sprite.width, sprite.height, power_up.pos_x, power_up.pos_y, True)

        player_sprite = player_sprites[game_state.player.curr_sprite]
        self.screen.blit(player_sprite, 0, 0, player_sprite.width, player_sprite.height, game_state.player.pos_x,
                         game_state.player.pos_y, True)
        if game_state.player.shield and game_state.player.shield.strength > 0:
            self.screen.blit(power_ups_sprites[0], 0, 0, power_ups_sprites[0].width, power_ups_sprites[0].height,
                             game_state.player.shield.pos_x,
                             game_state.player.shield.pos_y, True)

        base_x = 460
        base_y = 15
        i = 0
        while i < game_state.player.lives:
            self.screen.blit(life, 0, 0, life.width, life.height, base_x, base_y,
                             True)
            base_x = base_x - 20
            i += 1

        game_font.drawText(self.screen, 350, 10, str(game_state.level) + ui_const.LEVEL_LABEL)
        game_font.drawText(self.screen, 225, 10, str(game_state.score) + ui_const.SCORE_LABEL)
        game_font.drawText(self.screen, 75, 10, str(game_state.enemies_level) + ui_const.NEXT_LEVEL)
        self.screen.swap()

    def init_game(self, background, game_title, game_font):
        """
        Renders the initial scene of the game.

        Parameters
        ----------
        background: Image
            The background image.
        game_title: Image
            The game title image.
        game_font: Font
            The font used to display messages and labels.

        Returns
        -------
        None
        """
        self.screen.blit(background)
        self.screen.blit(game_title, 0, 0, game_title.width, game_title.height, 38, 100,
                         True)
        game_font.drawText(self.screen, 200, 200, ui_const.PRESS_START_MESSAGE)

        self.screen.swap()

    def end_game(self, score, background, game_title, game_font):
        """
        Renders the game over scene.

        Parameters
        ----------
        score: int
            The current score.
        background: Image
            The background image.
        game_title: Image
            The game title image.
        game_font: Font
            The font used to display messages and labels.
        Returns
        -------
        None
        """
        self.screen.blit(background)
        self.screen.blit(game_title, 0, 0, game_title.width, game_title.height, 38, 100,
                         True)
        game_font.drawText(self.screen, 210, 150, ui_const.GAME_OVER_MESSAGE)
        game_font.drawText(self.screen, 185, 180, ui_const.FINAL_SCORE_LABEL + str(score))
        game_font.drawText(self.screen, 200, 210, ui_const.RETRY_MESSAGE)

        self.screen.swap()
