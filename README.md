![](/misc/pocket_space_hero.png)

***

The future of the Earth is in danger! Are you ready to become the hero who will protect us from this alien threat?

## About

**Pocket Space Hero** is an open source game for Sony PSP programmed in Python using PSP-StacklessPython. 
Destroy alien spaceships as many as you can and compare your final score with your friends!


## Screenshots

![](/misc/init_game.png)

![](/misc/game.png)

![](/misc/end_game.png)

## How to play on PSP



## Support
For bugs, you can open an issue here on GitLab. For support (e.g. installation of the game), you can join the Discord server of my YouTube channel Tecnolstalgia https://discord.gg/9EcTVRZMDZ and ask at #pocket-space-hero.

## Contributing
Testing of the game and reporting bugs is well-accepted.

## License
The source code of the game is licensed under the GNU GPLv3. Assets like fonts and music:

Font [Futurespore](https://www.dafont.com/futurespore.font) by Andrew Young, Disaster Fonts is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 

Font [Reality Hyper](https://www.dafont.com/reality-hyper.font) (also known as Reality 64) by Grand Chaos Productions is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) and SIL Open Font License 1.1

Music Smash It by Shane Ivers - https://www.silvermansound.com is licensed under [CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1)

## Credits

Special thanks to:

Kenney www.kenney.nl for the amazing [graphics](https://opengameart.org/content/space-shooter-art)


## Resources

To know more about PSP-StacklessPython, check out these links [here](https://github.com/carlosedp/PSP-StacklessPython) and [here](https://code.google.com/archive/p/pspstacklesspython/).